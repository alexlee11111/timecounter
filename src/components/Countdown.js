import React from "react";
import '../styles/Countdown.css';

//public variable
var counting;

export default class Countdown extends React.Component {

    constructor(props) {
        super(props);

        var initTime = this.props.remainingSecond;
        console.log('Props: '+ this.props.remainingSecond);

        var hours   = Math.floor(initTime / 3600);
        console.log('H: '+hours);

        var minutes = Math.floor((initTime - (hours * 3600)) / 60);
        console.log('M: '+minutes);

        var seconds = initTime - (hours * 3600) - (minutes * 60);
        console.log('S: '+seconds);
    
        if (hours   < 10) {hours   = "0"+hours;}
        if (minutes < 10) {minutes = "0"+minutes;}
        if (seconds < 10) {seconds = "0"+seconds;}

        this.state = {
            time: initTime,
            control: false,
            h: hours,
            m: minutes,
            s: seconds
        }

        this.timerStart = this.timerStart.bind(this);
    }

    msCounte = () => {
        /*dfine timer variables*/
        var updateTime = this.state.time - 1;
        var hours   = Math.floor(updateTime / 3600);
        var minutes = Math.floor((updateTime - (hours * 3600)) / 60);
        var seconds = updateTime - (hours * 3600) - (minutes * 60);
    
        if (hours   < 10) {hours   = "0"+hours;}
        if (minutes < 10) {minutes = "0"+minutes;}
        if (seconds < 10) {seconds = "0"+seconds;}

        /*update timer state*/
        this.setState({
            time: updateTime,
            control: true,
            h: hours,
            m: minutes,
            s: seconds
        })

        console.log(hours+' '+minutes+' '+seconds);

    }

    timerStart = () => {

        var timer = this.myTimer;

        /*Start timer*/
        if(this.state.control == false){
            timer.counting = setInterval(this.msCounte, 1000);
        }
    }

    componentDidUpdate(){

        var timer = this.myTimer;

        /*End of the timer*/
        if((this.state.h==0) && (this.state.m==0) && (this.state.s==0)){
            console.log('end');
            clearInterval(timer.counting);
            if(this.props.onComplete){
                setTimeout(
                    () => {
                        this.props.onComplete();
                    }
                ,500);
            }
        }

        /*Sliding animation*/
        if((this.state.h>=0) && (this.state.m>=0) && (this.state.s>=0)){
            
            timer.className += 'Countdown-timer';
            setTimeout(
                () => {
                    timer.className = '';
                },800
            )
        }
    }

    render() {
        return (
            <div>
                <div ref= {timer=>{this.myTimer = timer}}>
                    <p>{this.state.h}:{this.state.m}:{this.state.s}</p>
                </div>
                <button onClick={this.timerStart}>Click</button>
            </div>
        );
    }
}