import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Countdown from './components/Countdown';

//You may udate this variable to change the timer of the 2nd timer
var number = 10;

class App extends Component {
  constructor(){
    super();
    this.state = {
      number: undefined
    }
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        <Countdown remainingSecond={240}/>
        {
          number != undefined?
          <Countdown remainingSecond={number} onComplete={() => alert("Done")}/>
          :
          null
        }
      </div>
    );
  }
}

export default App;
